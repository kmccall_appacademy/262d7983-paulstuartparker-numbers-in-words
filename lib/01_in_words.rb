

class Fixnum
  def in_words
    words = self.to_words
    if words == ""
      return "zero"
    else
      words
    end
  end

  def to_words
    int = self
    length = int.to_s.length
    digit_arr = digits(int)
    result = []

    if int > 999999999999
      result << hundreds(digit_arr[0..(digits_above(int) - 1)].map { |x| x.to_s}.join.to_i)
      result << "trillion"
      result << (digit_arr[digits_above(int)..-1].map { |x| x.to_s}.join.to_i).to_words
      puts digits_above(int)
    elsif int > 999999999
      result << hundreds(digit_arr[0..(digits_above(int) - 1)].map { |x| x.to_s}.join.to_i)
      result << "billion"
      result << (digit_arr[digits_above(int)..-1].map { |x| x.to_s}.join.to_i).to_words
    elsif int > 999999
      result << hundreds(digit_arr[0..(digits_above(int) - 1)].map { |x| x.to_s}.join.to_i)
      result << "million"
      result << (digit_arr[digits_above(int)..-1].map { |x| x.to_s}.join.to_i).to_words
    elsif int > 999
      result << hundreds(digit_arr[0..(digits_above(int) - 1)].map { |x| x.to_s}.join.to_i)
      result << "thousand"
      puts result
      result << (digit_arr[digits_above(int)..-1].map { |x| x.to_s}.join.to_i).to_words
    elsif int <= 999
      result << hundreds(digit_arr.map { |x| x.to_s}.join.to_i)
    end


  result.join(" ").strip
  end


  private

  def digits_above(int)
    if (int.to_s.split("").count) % 3 == 0
      return 3
    else
      (int.to_s.split("").count) % 3
    end
  end

  def digits(int)
    int.to_s.split("").map { |x| x.to_i}
  end

  def hundreds(int)
    teens = {
    19=>"nineteen",
    18=>"eighteen",
    17=>"seventeen",
    16=>"sixteen",
    15=>"fifteen",
    14=>"fourteen",
    13=>"thirteen",
    12=>"twelve",
    11 => "eleven",
    10 => "ten"
    }
    ones = {
    9 => "nine",
    8 => "eight",
    7 => "seven",
    6 => "six",
    5 => "five",
    4 => "four",
    3 => "three",
    2 => "two",
    1 => "one",
    }
    tens = {
    90 => "ninety",
    80 => "eighty",
    70 => "seventy",
    60 => "sixty",
    50 => "fifty",
    40 => "forty",
    30 => "thirty",
    20 => "twenty",
    10 => "ten"
    }
    result = []
    digit_arr = digits(int)
    if int > 99
      result << ones[digit_arr[0]]
      result << "hundred"
        if digit_arr[1] * 10 > 19
          result << tens[(digit_arr[1] * 10)]
          result << ones[digit_arr[2]] unless digit_arr[2] == 0
        elsif digit_arr[1, 2].map { |x| x.to_s}.join.to_i > 10
          result << teens[digit_arr[1, 2].map { |x| x.to_s}.join.to_i]
        elsif digit_arr[1, 2].map { |x| x.to_s}.join.to_i < 10
          result << ones[digit_arr[2]]
        end
    elsif int >= 20
      result << tens[(digit_arr[0] * 10)]
      result << ones[digit_arr[1]] if digit_arr[1] != 0
    elsif int >= 10
      result << teens[int]
    else
      result << ones[int] unless int == 0
    end
  result.join(" ").strip
  end
end
